package edu.mit.aiti.india.lab2;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import edu.mit.aiti.india.lab2.StateDetailActivity.StateDetailFetcher;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class StateMapActivity extends Activity {
	// This class takes a URL as its argument and passes a Bitmap back to the UI thread.
	public class StateMapFetcher extends AsyncTask<URL, Integer, Bitmap> {

		@Override
		protected Bitmap doInBackground(URL... urls) {
			// Prepare the object out here in case of exception.
			Bitmap map = null;
			HttpURLConnection conn;
			InputStream in;
			try {
				conn = (HttpURLConnection) urls[0].openConnection();
				
				// Open and read the InputStream as a String (via a Reader)
				in = conn.getInputStream();
			} catch (IOException e) {
				// If we failed this early, we never connected.
				return map;
			}
			
			// If we did not fail, we can try reading from the connection.
			try {
				// Read the InputStream into a ByteArrayOutputStream so we can turn it into a byte[] array.
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				// Read into a buffer.
				byte[] byteBuffer = new byte[256];
				while (true) {
					int bytesRead = in.read(byteBuffer);
					if (bytesRead < 0) {
						break;
					}
					// Write out only those bytes that were read.
					out.write(byteBuffer, 0, bytesRead);
				}
				byte[] data = out.toByteArray();
				
				// Finally, convert the byte array into a Bitmap.
				BitmapFactory.Options options = new BitmapFactory.Options();
				map = BitmapFactory.decodeByteArray(data, 0, data.length, options);
			} catch (IOException e) {
				// Can't do anything.
			} finally {
				// ALWAYS disconnect, even if we caught an exception.
				conn.disconnect();
			}
			return map;
		}
		
		public void onPostExecute(final Bitmap map) {
			// This is running on the UI thread.
			
			// We can finally set the ImageView.
	        ImageView imageMap =
	        		(ImageView) findViewById(R.id.imageMap);
	        imageMap.setImageBitmap(map);
		}
	}

	// Hang on to the fetcher, as we may want to cancel it.
	private StateMapFetcher mFetcher;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_map);
        
        // Get the intent that started this activity to retrieve
        // the URI we want to fetch.
        Intent startingIntent = this.getIntent();
        String urlString = startingIntent.getStringExtra("URL");
        
        // Convert to a URL and fetch.
        try {
			this.mFetcher = new StateMapFetcher();
			this.mFetcher.execute(new URL(urlString));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.state_detail_menu, menu);
        // Check the right item.
        menu.findItem(R.id.menu_detail).setChecked(false);
        menu.findItem(R.id.menu_map).setChecked(true);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_detail:
            	// Destroy this activity.
            	this.finish();
            	return true;
            case R.id.menu_map:
            	// Do nothing.
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
