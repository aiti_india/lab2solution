package edu.mit.aiti.india.lab2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import edu.mit.aiti.india.lab2.ListStatesActivity.StateListFetcher;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.support.v4.app.NavUtils;

public class StateDetailActivity extends Activity {
	// This class takes a URL as its argument and passes a JSONObject back to the UI thread.
	public class StateDetailFetcher extends AsyncTask<URL, Integer, JSONObject> {

		@Override
		protected JSONObject doInBackground(URL... urls) {
			// Prepare the object out here in case of exception.
			JSONObject object = new JSONObject();
			HttpURLConnection conn;
			InputStream in;
			try {
				conn = (HttpURLConnection) urls[0].openConnection();
				
				// Open and read the InputStream as a String (via a Reader)
				in = conn.getInputStream();
			} catch (IOException e) {
				// If we failed this early, we never connected.
				return object;
			}
			
			// If we did not fail, we can try reading from the connection.
			try {
				// The InputStreamReader turns the bytes of the InputStream into characters (it decodes using the "UTF-8" encoding)
				InputStreamReader inReader = new InputStreamReader(in, "UTF8");
				
				// The following is an alternative way to read from a reader, rather than reading by line.
				// You can do something similar to read binary data like images (which we do in StateMapActivity)
				
				// We then set up a temporary buffer which .read() will save the read characters to.
				// We have the size at 256 characters, but it could be anything.  Note that larger buffers will take longer to fill though.
				char[] charBuffer = new char[256];
				// The StringBuffer allows us to easily build the String object, one set of characters at a time.
				StringBuffer stringBuffer = new StringBuffer();
				// Until we no longer have any characters to read...
				while (true) {
					// Read up to 256 characters from the InputStream (via InputStreamReader)
					int charsRead = inReader.read(charBuffer);
					// If we are at the end of the stream (charsRead == -1) then we can exit.
					if (charsRead < 0) {
						break;
					}
					// And add the characters read into charBuffer to stringBuffer.
					// Here we provide the number of characters read so that, if we read less than 256 characters at once, only the new characters are added.
					stringBuffer.append(charBuffer, 0, charsRead);
				}
				// Finally we convert it to a string and disconnect from the HttpURLConnection.
				String jsonString = stringBuffer.toString();
				
				// Read as JSON, expect an object.
				// Save it as a member variable for later use.
				object = new JSONObject(jsonString);
			} catch (IOException e) {
				// Can't do anything.
			} catch (JSONException e) {
				// Can't do anything.
			} finally {
				// ALWAYS disconnect, even if we caught an exception.
				conn.disconnect();
			}
			return object;
		}
		
		public void onPostExecute(final JSONObject object) {
			// This is running on the UI thread.
			
			// Now, populate the fields.
			// Again, we can use findViewById directly because this class is declared INSIDE the StateDetailActivity class.
			// Thus it calls the findViewById of the StateDetailActivity.
			try {
				TextView textName = (TextView) findViewById(R.id.textName);
				textName.setText(object.getString("name"));
			} catch (JSONException e) {
				// Oh well.
			}

			try {
				TextView textCapital = (TextView) findViewById(R.id.textCapital);
				textCapital.setText(object.getString("capital"));
			} catch (JSONException e) {
				// Oh well.
			}

			try {
				TextView textLanguages = (TextView) findViewById(R.id.textLanguages);
				
				// Here we need to actually extract and concatenate an array.
				StringBuffer languageList = new StringBuffer();
				JSONArray languageArray = object.getJSONArray("languages");
				for (int i = 0; i < languageArray.length(); i++) {
					if (i + 1 == languageArray.length()) {
						languageList.append(" and ");
					} else if (i > 0) {
						languageList.append(", ");
					}
					languageList.append(languageArray.getString(i));
				}
				
				textLanguages.setText(languageList.toString());
			} catch (JSONException e) {
				// Oh well.
			}

			try {
				TextView textPopulation = (TextView) findViewById(R.id.textPopulation);
				// Here is something new! DecimalFormat helps to format decimal numbers (e.g. adding commas in the right places)
				DecimalFormat formatter = new DecimalFormat();
				textPopulation.setText(formatter.format(object.getLong("population")));
			} catch (JSONException e) {
				// Oh well.
			}

			try {
				TextView textArea = (TextView) findViewById(R.id.textArea);
				// Again we use DecimalFormat.
				DecimalFormat formatter = new DecimalFormat();
				textArea.setText(formatter.format(object.getDouble("area")));
			} catch (JSONException e) {
				// Oh well.
			}

			// FINALLY: We save the object to the activity's member field, so that onOptionsItemSelected can work.
			StateDetailActivity.this.mStateData = object;
		}
	}

	// Hang on to the fetcher, as we may want to cancel it.
	private StateDetailFetcher mFetcher;
	private JSONObject mStateData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_detail);
        
        // Get the intent that started this activity to retrieve
        // the URL we want to fetch from its extra arguments.
        Intent startingIntent = this.getIntent();
        // We stored it under the "URL" key.
        String urlString = startingIntent.getStringExtra("URL");
        
        // Convert to a URL and start the AsyncTask.
        try {
			this.mFetcher = new StateDetailFetcher();
			this.mFetcher.execute(new URL(urlString));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.state_detail_menu, menu);
        // Check the right item.
        menu.findItem(R.id.menu_detail).setChecked(true);
        menu.findItem(R.id.menu_map).setChecked(false);
        return true;
    }

    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.menu_detail:
            	// Do nothing.
            	return true;
            case R.id.menu_map:
            	// Start the map activity.
            	/* Create the intent to start StateDetailActivity from this activity. */
            	Intent startStateMapActivityIntent =
            			new Intent(
            					StateDetailActivity.this,
            					StateMapActivity.class);
            	// And the intent should have, as an extra argument, the URL of the state data.
            	try {
					startStateMapActivityIntent.putExtra("URL",
							mStateData.getString("map"));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return true;
				} catch (NullPointerException e) {
					// This will be thrown if mStateData hasn't been set yet (it is null).
					e.printStackTrace();
					return true;
				}

            	startActivity(startStateMapActivityIntent);
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
