package edu.mit.aiti.india.lab2;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.support.v4.app.NavUtils;

public class ListStatesActivity extends Activity {
	// This class takes a URL as its argument and passes a JSONArray back to the UI thread.
	public class StateListFetcher extends AsyncTask<URL, Integer, JSONArray> {

		@Override
		protected JSONArray doInBackground(URL... urls) {
			// Prepare the array out here in case of exception.
			JSONArray array = new JSONArray();
			HttpURLConnection conn;
			InputStream in;
			try {
				conn = (HttpURLConnection) urls[0].openConnection();
				
				// Open and read the InputStream as a String (via a Reader)
				in = conn.getInputStream();
			} catch (IOException e) {
				// If we failed this early, we never connected.
				return array;
			}
			
			// If we did not fail, we can try reading from the connection.
			try {
				// The InputStreamReader turns the bytes of the InputStream into characters (it decodes using the "UTF-8" encoding)
				InputStreamReader inReader = new InputStreamReader(in, "UTF8");
				
				// The following is an alternative way to read from a reader, rather than reading by line.
				// You can do something similar to read binary data like images (which we do in StateMapActivity)
				
				// We then set up a temporary buffer which .read() will save the read characters to.
				// We have the size at 256 characters, but it could be anything.  Note that larger buffers will take longer to fill though.
				char[] charBuffer = new char[256];
				// The StringBuffer allows us to easily build the String object, one set of characters at a time.
				StringBuffer stringBuffer = new StringBuffer();
				// Until we no longer have any characters to read...
				while (true) {
					// Read up to 256 characters from the InputStream (via InputStreamReader)
					int charsRead = inReader.read(charBuffer);
					// If we are at the end of the stream (charsRead == -1) then we can exit.
					if (charsRead < 0) {
						break;
					}
					// And add the characters read into charBuffer to stringBuffer.
					// Here we provide the number of characters read so that, if we read less than 256 characters at once, only the new characters are added.
					stringBuffer.append(charBuffer, 0, charsRead);
				}
				// Finally we convert it to a string and disconnect from the HttpURLConnection.
				String jsonString = stringBuffer.toString();
				
				// Read as JSON, expect an array.
				// Save it as a member variable for later use.
				array = new JSONArray(jsonString);
			} catch (IOException e) {
				// Can't do anything.
			} catch (JSONException e) {
				// Can't do anything.
			} finally {
				// ALWAYS disconnect, even if we caught an exception.
				conn.disconnect();
			}
			return array;
		}
		
		public void onPostExecute(final JSONArray array) {
			// This is running on the UI thread.
			
			// Get an array of state names from each object's "name" property.
			ArrayList<String> stateNames = new ArrayList<String>(array.length());
			for (int i = 0; i < array.length(); i++) {
				try {
					stateNames.add(array.getJSONObject(i).getString("name"));
				} catch (JSONException e) {
					// Nothing we can do.
					e.printStackTrace();
				}
			}
			
	        // Set up the ListView's adapter.
			// Note that we can call findViewById directly here, because this class is declared INSIDE ListStatesActivity.
			// It thus has access to all of ListStatesActivity's methods, including findViewById
	        ListView listStates =
	        		(ListView) findViewById(R.id.listStates);
	        ArrayAdapter<String> stateList = new ArrayAdapter<String>(
	        		ListStatesActivity.this,
	        		android.R.layout.simple_list_item_1,
	        		stateNames);
	        listStates.setAdapter(stateList);
	        
	        // And set the OnItemClickListener to start up StateDetailActivity
	        listStates.setOnItemClickListener(new OnItemClickListener() {
	        	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            	/* Create the intent to start StateDetailActivity from this activity. */
	            	Intent startStateDetailActivityIntent =
	            			new Intent(
	            					ListStatesActivity.this,
	            					StateDetailActivity.class);
	            	// And the intent should have, as an extra argument, the URL (URI) of the state data.
	            	try {
	            		// Note that we can still refer to "array" because it is declared "final".
						startStateDetailActivityIntent.putExtra("URL",
								array.getJSONObject(position).getString("url"));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return;
					}

	            	startActivity(startStateDetailActivityIntent);
	        	}
	        });

		}
	}

	// Hang on to the fetcher, as we may want to cancel it.
	private StateListFetcher mFetcher;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_states);
        
        // Hang on to the fetcher so we can access 
        this.mFetcher = new StateListFetcher();
        try {
        	this.mFetcher.execute(new URL("http://telegraphis.net/demoapps/aiti_india/india.json"));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_list_states, menu);
        return true;
    }

    
}
